##### license

the licenses for the contents of this directory are various, but to our knowledge
all of them allow redistribution in this manner. All of these were linked against the AOSP
implementations, and their presence in this repository shouldn't be taken to suggest otherwise.

##### attribution

`demo_app.apk` - friends of atl (source available if you care, probably 1:1 identical to jadx decompilation)  
`gles3jni.apk` - google example (https://github.com/android/ndk-samples/)  
`sample_sdl2_app.apk` - friends of atl (source available on gitlab)  
`views_widgets_samples_flower_finder.apk` - google example (https://github.com/android/views-widgets-samples/tree/main/RecyclerViewKotlin)  
`ndk_sample_endless_tunnel.apk` - google example (https://github.com/android/ndk-samples/)  
`oculus_vr_gl2hittestOXR.apk` - https://github.com/terryky/android_openxr_gles/  
`oculus_vr_gl2tri3dOXR.apk` - https://github.com/terryky/android_openxr_gles/  
`org.happysanta.gd_29.apk` - https://github.com/evgenyzinoviev/gravitydefied/, patched  
`sample_unity_app.apk` - friends of atl (and unity runtime)  
`glmark2.apk` - https://github.com/glmark2/glmark2/releases/tag/2023.01, patched  
`com.ashwin.example.accelerometerdemo.apk` - https://github.com/ashwindmk/Android-Accelerometer-Demo/  
`compose_demo_app.apk` - https://github.com/puritanin/ComposeDemoApp  
`com.leocardz.multitouch.test_19.apk` - https://github.com/LeonardoCardoso/Multitouch-Test/ (apk from f-droid archive)
